class Image < ActiveRecord::Base
  FIT_WIDTH = 150
  TAX_FORMATS = ['gif']

  RESIZE_TAX = 0.001
  SIZE_TAX = 0.001
  FORMAT_TAX = 0.005

  SIZE_LIMIT = 100_000

  attr_accessor :file

  validates :name, presence: true,
                   length: { in: 3..255 }
  validates :url,  presence: true
  validates :file, image: true, unless: :any_errors?

  after_validation :set_path
  before_save :image_info
  after_save :resize_image


  def calc_price
    price = RESIZE_TAX
    price += SIZE_TAX if size > SIZE_LIMIT
    price += FORMAT_TAX if TAX_FORMATS.include?(self.format)
    price
  end

  protected

    def any_errors?
      errors.any?
    end

    def set_path
      ts = Time.now.to_i
      self.path = File.join('images', "#{ts}_#{name}.#{file.format.downcase}")
    end

    def image_info
      self.format = file.format.downcase
      self.size = file.filesize
    end

    def resize_image
      file.resize_to_fit!(FIT_WIDTH, nil)
    end

end
