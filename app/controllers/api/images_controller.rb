require 'RMagick'
class Api::ImagesController < ApplicationController
  include Api::ImagesHelper
  def index
    images = []
    Image.all.each do |image|
      images << { name: image.name, image: full_image_path(image.path),
                  price: image.calc_price }
    end
    render json: images, status: :ok
  end

  def create
    image = Image.new(image_params.merge(file: Magick::Image::read(params[:url]).first))

    if image.valid?
      image.file.write(image.path)
      image.save
      render json: { status: 'ok' }, status: :created
    else
      render json: { errors: image.errors }, status: :unprocessable_entity
    end

  rescue Magick::ImageMagickError => e
    render json: { errors: { url: ["Can't access #{params[:url]}"] } },
           status: :unprocessable_entity
  end

  private

    def image_params
      params.permit(:name, :url)
    end
end
