class ImageValidator < ActiveModel::EachValidator
  ALLOWED_FORMATS = %w[png gif jpeg]

  def validate_each(record, attribute, value)
    unless ALLOWED_FORMATS.include?(value.format.downcase)
      record.errors[attribute] << (options[:message] || 'has wrong format')
    end
  end
end