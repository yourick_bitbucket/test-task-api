module Api::ImagesHelper
  def full_image_path(image)
    "#{request.protocol}#{request.host_with_port}/#{image}"
  end
end
