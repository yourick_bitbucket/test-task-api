class CreateTableImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :name
      t.string :url
      t.string :format
      t.integer :size
      t.string :path
      t.timestamps null: false
    end
  end
end
